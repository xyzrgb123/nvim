" УСТАНОВКА ПЛАГИНОВ{{{1
call plug#begin('~/.config/nvim/plugged')
" Оформление {{{2

" цветовые схемы {{{3

Plug 'ghifarit53/tokyonight-vim' " tokyonight
Plug 'morhetz/gruvbox' " gruvbox
Plug 'damage220/solas.vim' " solas
Plug 'nanotech/jellybeans.vim' " jellybeans 
Plug 'chriskempson/tomorrow-theme'
Plug 'henrynewcomer/vim-theme-papaya' " papaya
Plug 'gilgigilgil/anderson.vim' " anderson
Plug 'tomasiser/vim-code-dark' " codedark
Plug 'ErichDonGubler/vim-sublime-monokai' " sublimemonokai
Plug 'altercation/vim-colors-solarized' " solarized
Plug 'joshdick/onedark.vim' " onedark
Plug 'mhartington/oceanic-next' " OceanicNext
Plug 'lifepillar/vim-solarized8' " solarized8_flat
Plug 'alessandroyorba/alduin' " alduin
Plug 'duythinht/vim-coffee' " coffee

" }}}

" Тема в зависимости от типа файла
" Plug 'caglartoklu/ftcolor.vim'

" Строка состояния
Plug 'vim-airline/vim-airline'

" Темы для строки состояния
Plug 'vim-airline/vim-airline'

" Отобразить метки
Plug 'kshenoy/vim-signature'

" Отображение отступов
Plug 'nathanaelkane/vim-indent-guides'

" Написание текста {{{2

" Автокомплит
Plug 'lifepillar/vim-mucomplete'

" Сниппеты
" Plug 'honza/vim-snippets'
" Plug 'SirVer/ultisnips'
Plug 'L3MON4D3/LuaSnip'

" Emmet
Plug 'mattn/emmet-vim'

" Комментирование
Plug 'tpope/vim-commentary'

" Изменить обрамление
Plug 'tpope/vim-surround'

" Автопара ковычек/скобок
Plug 'jiangmiao/auto-pairs'

" Навигация по тексту {{{2

" Перемещалка
Plug 'easymotion/vim-easymotion'

" Плавная прокрутка
Plug 'yuttie/comfortable-motion.vim'

" Навигация по файлам {{{2

" Файловый менеджер
Plug 'francoiscabrol/ranger.vim'

" Закрывашка
Plug 'rbgrouleff/bclose.vim'

" Буфер
Plug 'sandeepcr529/buffet.vim'

" Поиск по файлам
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'

" Git {{{2

" Терминал git
Plug 'tpope/vim-fugitive'

" Подсветка git-строк
Plug 'airblade/vim-gitgutter'

" Поддержка языков {{{2

" Stylus
Plug 'iloginow/vim-stylus' " подсветка и автокомплит

" Pug
Plug 'dnitro/vim-pug-complete' " автокомплит

" Pug
Plug 'digitaltoad/vim-pug' " подсветка

" Coffee
Plug 'kchmck/vim-coffee-script'

call plug#end()
" РЕМАПЫ {{{1
" ВСЯКОЕ {{{2
let mapleader = " " " Leader
inoremap kj <Esc>
noremap <F10> <F1>
nnoremap <leader>/ :noh<CR>
" раскладка {{{2
map ё `
map й q
map ц w
map у e
map к r
map е t
map н y
map г u
map ш i
map щ o
map з p
map х [
map ъ ]
map ф a
map ы s
map в d
map а f
map п g
map р h
map о j
map л k
map д l
map ж ;
map э '
map я z
map ч x
map с c
map м v
map и b
map т n
map ь m
map б ,
map ю .
map Ё ~
map Й Q
map Ц W
map У E
map К R
map Е T
map Н Y
map Г U
map Ш I
map Щ O
map З P
map Х {
map Ъ }
map Ф A
map Ы S
map В D
map А F
map П G
map Р H
map О J
map Л K
map Д L
map Ж :
map Э "
map Я Z
map Ч X
map С C
map М V
map И B
map Т N
map Ь M
map Б <
map Ю >
" }}}
" ПЕРЕМЕЩЕНИЕ {{{2
noremap ' ;
noremap k j
noremap ; l
noremap l k
noremap j h
noremap <leader>h 0
noremap <leader>j ^
noremap <leader>; $
noremap <C-k> }
noremap <C-l> {
inoremap <C-k> <left>
inoremap <C-l> <right>
noremap <leader>m M
noremap <leader>k L
noremap <leader>l H
" КОПИПАСТА {{{2
inoremap <leader>p <ESC>"+pa
vnoremap <leader>y "+y
noremap <leader>w :w<CR>
noremap <leader>x :x<CR>

" ПЕРЕМЕЩЕНИЕ МЕЖДУ ОКНАМИ {{{2
function! WinMove(key)
	let t:curwin = winnr()
	exec "wincmd ".a:key
	if (t:curwin == winnr())
		if (match(a:key,'[jk]'))
			wincmd v
		else
			wincmd s
		endif
		exec "wincmd ".a:key
	endif
endfunction

map <silent> <A-j> :call WinMove('h')<CR>
map <silent> <A-k> :call WinMove('j')<CR>
map <silent> <A-l> :call WinMove('k')<CR>
map <silent> <A-;> :call WinMove('l')<CR>

" ПЕРЕКЛЮЧЕНИЕ НУМЕРАЦИИ СТРОК {{{2
function! NemberToggle()
    if(&relativenumber == 1)
        set nornu
        set number
    else
        set rnu
    endif
endfunc

nnoremap  <leader>r :call NemberToggle()<CR>

" МАПЫ ПЛАГИНОВ {{{1

" bclose (Закрывашка)
noremap <leader>q :Bclose<CR>

" buffet (буфер)
noremap <leader>a :Bufferlist<CR>

" ranger (файловый менеджер)
noremap <leader>z :Ranger<CR>

" easymotion (перемещалка) 
" на символ
map  <Leader>v <Plug>(easymotion-bd-f)
nmap <Leader>v <Plug>(easymotion-overwin-f)
" на двойной символ
nmap <Leader>d <Plug>(easymotion-overwin-f2)
" на строку
map <Leader>s <Plug>(easymotion-bd-jk)
nmap <Leader>s <Plug>(easymotion-overwin-line)

" emmet
" let g:user_emmet_expandabbr_key = '<C-j>'

" ultisnips (сниппеты)
" открыть
" let g:UltiSnipsExpandTrigger='<tab>'
" следующий аргумент
" let g:UltiSnipsJumpForwardTrigger="<C-b>"
" предъидущий аргумент. НЕ РАБОТАЕТ
" let g:UltiSnipsJumpBackwardTrigger="<C-z>"

comfortable-motion (плавная прокрутка)
" вниз
nnoremap <silent> <C-i> :call comfortable_motion#flick(200)<CR>
" вверх
nnoremap <silent> <C-o> :call comfortable_motion#flick(-200)<CR>

" indent-guides (подсветка отступов)
map <F5> :IndentGuidesToggle<CR>

" SET-ы {{{1
" Оформление {{{2
colorscheme jellybeans
set background=dark
syntax on " включить подсветку синтаксиса
set number relativenumber " относительная нумерация строк
set cursorline " обозначение строки курсора
set cursorcolumn " обозначение столбца курсора
set t_Co=256 " поддержка цветов

" Табы {{{2

set tabstop=4 " длинна таба
set shiftwidth=4 " длинна добавляемого строке таба
set softtabstop=4 " длинна добавляемого таба
set expandtab " в режиме вставки заменяет символ табуляции на соответствующее количество пробелов

" Поиск {{{2

set ignorecase " игнорить регистр при поиске
set smartcase " умный регистр при поиске
set hlsearch " подсветка результатов поиска
set incsearch " подсказка первого вхождения при наборе шаблона поиска
set is " использовать инкрементальный поиск

" Копипаста {{{2

set clipboard=unnamedplus
set clipboard+=unnamedplus " копирование в системный буфер

" Кодировка {{{2

set fileencoding=utf-8 " кодировка записываемого файла
set encoding=utf-8 " кодировка вывода в терминале
set termencoding=utf-8

" Автокомплит {{{2

set completeopt+=menuone " Для автокомплита
" set completeopt+=noselect
set completeopt+=noinsert
set shortmess+=c   " shut off completion messages

" Перенос слов {{{2

set nowrap " без переноса строк
set linebreak " перенос по словам, а не по буквам
set dy=lastline

" Раскладка {{{2

" Не игнорь кирилицу
" set langmap=ёйцукенгшщзхъфывапролджэячсмитьбю;`qwertyuiop[]asdfghjkl\;'zxcvbnm\,.,ЙЦУКЕHГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮ;QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>
set iminsert=0 " по умолчанию - латинская раскладка
set imsearch=0 " по умолчанию - латинская раскладка при поиске

" Файл {{{2

set undofile " сохранение изменений после перезагрузки
set backupdir=/tmp " бэкап
set directory=~/.nvim/tmp,.

" Прочее {{{2

set foldmethod=marker " метод сворачивания текста = по маркерам
set showcmd " отображение выполняемой команды
set mouse+=a " поддержка мышки

" НАСТРОЙКИ ПЛАГИНОВ {{{1

" airline (строка состояния)
let g:airline_theme='onedark'

" mucomplete (автокомплит)
let g:mucomplete#enable_auto_at_startup = 1

" ultisnips (сниппеты)
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]

" ftcolor (тема в зависимости от типа файла)
" let g:ftcolor_redraw = 1
" let g:ftcolor_default_color_scheme = 'tokyonight'
" let g:ftcolor_color_mappings = {}
" let g:ftcolor_color_mappings.javascript = 'jellybean'
" let g:ftcolor_color_mappings.coffee = 'jellybean'
" let g:ftcolor_color_mappings.python = 'gruvbox'
" let g:ftcolor_color_mappings.html = 'codedark'
" let g:ftcolor_color_mappings.pug = 'codedark'
" let g:ftcolor_color_mappings.css = 'onedark'
" let g:ftcolor_color_mappings.stylus = 'onedark'
" let g:ftcolor_color_mappings.less = 'onedark'
" let g:ftcolor_color_mappings.scss = 'onedark'
" let g:ftcolor_color_mappings.sass = 'onedark'

" МАКРОСЫ {{{1

" Фолд
let @c='i{{{kjyypOkjokjkxxxgcclll;;;xxxgcc'
map <F6> @c
" }}}

